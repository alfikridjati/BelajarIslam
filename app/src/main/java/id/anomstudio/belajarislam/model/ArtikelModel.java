package id.anomstudio.belajarislam.model;

public class ArtikelModel {

    private String id;
    private String kategori;
    private String judul;
    private String deskripsi;

    public ArtikelModel(){
        //this constructor is required
    }

    public ArtikelModel(String id, String kategori, String judul, String deskripsi) {
        this.id = id;
        this.kategori = kategori;
        this.judul = judul;
        this.deskripsi = deskripsi;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKategori() {
        return kategori;
    }

    public void setKategori(String kategori) {
        this.kategori = kategori;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }
}
