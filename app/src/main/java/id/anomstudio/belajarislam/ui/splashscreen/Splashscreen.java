package id.anomstudio.belajarislam.ui.splashscreen;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.pixplicity.sharp.Sharp;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.anomstudio.belajarislam.ui.MainActivity;
import id.anomstudio.belajarislam.R;

public class Splashscreen extends AppCompatActivity {

    @BindView(R.id.logo)
    ImageView logo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);
        ButterKnife.bind(this);
        Sharp.loadResource(getResources(),R.raw.ic_logo).into(logo);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), MainActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                finish();
            }
        }, 2000);

    }
}
