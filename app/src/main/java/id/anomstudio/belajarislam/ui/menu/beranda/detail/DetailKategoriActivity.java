package id.anomstudio.belajarislam.ui.menu.beranda.detail;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.anomstudio.belajarislam.R;
import id.anomstudio.belajarislam.adapter.ArtikelListAdapter;
import id.anomstudio.belajarislam.model.ArtikelModel;
import id.anomstudio.belajarislam.ui.MainActivity;

public class DetailKategoriActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.titleToolbar)
    TextView titleToolbar;
    @BindView(R.id.back)
    ImageButton back;
    @BindView(R.id.input_search)
    EditText inputSearch;
    @BindView(R.id.search)
    ImageButton search;
    @BindView(R.id.list)
    RecyclerView list;
    @BindView(R.id.progress)
    ProgressBar progress;
    private List<ArtikelModel> artikelList;
    public static String fikih_doa_dzikir;
    public static String kumpulan_doa;
    public static String biografi_narasumber;
    private DatabaseReference databaseReference;
    private ArtikelListAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_kategori);
        ButterKnife.bind(this);
        databaseReference = FirebaseDatabase.getInstance().getReference("artikels");
        artikelList = new ArrayList<>();

        Bundle bundle = getIntent().getExtras();
        fikih_doa_dzikir = bundle.getString("fikih_doa_dzikir");
        kumpulan_doa = bundle.getString("kumpulan_doa");
        biografi_narasumber = bundle.getString("biografi_narasumber");

    }

    @Override
    protected void onStart() {
        super.onStart();
        if (fikih_doa_dzikir != null) {

            titleToolbar.setText(fikih_doa_dzikir);
            tampilArtikel(databaseReference.child("Fikih Doa dan Dzikir"));

        } else if (kumpulan_doa != null) {

            titleToolbar.setText(kumpulan_doa);
            tampilArtikel(databaseReference.child("Kumpulan Doa dan Dzikir"));

        } else if (biografi_narasumber != null) {

            titleToolbar.setText(biografi_narasumber);
            tampilArtikel(databaseReference.child("Biografi Narasumber"));

        }
    }

    private void tampilArtikel(DatabaseReference databaseReferences){
        databaseReferences.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                artikelList.clear();
                for (DataSnapshot artikelSnapshot : dataSnapshot.getChildren()){
                    ArtikelModel artikelModel = artikelSnapshot.getValue(ArtikelModel.class);
                    artikelList.add(artikelModel);
                }
                progress.setVisibility(View.GONE);
                list.setHasFixedSize(true);
                list.setLayoutManager(new LinearLayoutManager(DetailKategoriActivity.this));
                adapter = new ArtikelListAdapter(DetailKategoriActivity.this,artikelList);
                list.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    @OnClick(R.id.search)
    public void search(){
        titleToolbar.setVisibility(View.GONE);
        inputSearch.setVisibility(View.VISIBLE);
        inputSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                filter(s.toString());
            }
        });
    }

    private void filter(String text){
        ArrayList<ArtikelModel> filteredList = new ArrayList<>();

        for(ArtikelModel item : artikelList){
            if(item.getJudul().toLowerCase().contains(text.toLowerCase())){
                filteredList.add(item);
            }
        }

        adapter.filterList(filteredList);

    }

    @OnClick(R.id.back)
    public void back(){
        onBackPressed();
    }

}
