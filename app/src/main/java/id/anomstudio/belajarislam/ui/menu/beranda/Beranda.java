package id.anomstudio.belajarislam.ui.menu.beranda;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.pixplicity.sharp.Sharp;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.anomstudio.belajarislam.R;
import id.anomstudio.belajarislam.ui.menu.beranda.detail.DetailKategoriActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class Beranda extends Fragment {

    @BindView(R.id.image1)
    ImageView image1;
    @BindView(R.id.image2)
    ImageView image2;
    @BindView(R.id.image3)
    ImageView image3;
    @BindView(R.id.text1)
    TextView text1;
    @BindView(R.id.text2)
    TextView text2;
    @BindView(R.id.text3)
    TextView text3;

    public Beranda() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_beranda, container, false);
        ButterKnife.bind(this,v);
        Sharp.loadResource(getResources(), R.raw.ic_muslim_salat).into(image1);
        Sharp.loadResource(getResources(), R.raw.ic_muslim_book).into(image2);
        Sharp.loadResource(getResources(), R.raw.ic_muslim_mosque).into(image3);
        return v;
    }

//    @OnClick(R.id.button_add)
//    public void button_add(){
//        startActivity(new Intent(getActivity(), AddActivity.class));
//    }

    @OnClick(R.id.shadow1)
    public void shadow1(){
        Intent intent = new Intent(getActivity(),DetailKategoriActivity.class);
        intent.putExtra("fikih_doa_dzikir",text1.getText().toString());
        startActivity(intent);
    }

    @OnClick(R.id.shadow2)
    public void shadow2(){
        Intent intent = new Intent(getActivity(),DetailKategoriActivity.class);
        intent.putExtra("kumpulan_doa",text2.getText().toString());
        startActivity(intent);
    }

    @OnClick(R.id.shadow3)
    public void shadow3(){
        Intent intent = new Intent(getActivity(),DetailKategoriActivity.class);
        intent.putExtra("biografi_narasumber",text3.getText().toString());
        startActivity(intent);
    }


}
