package id.anomstudio.belajarislam.ui.menu.beranda.detail;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import id.anomstudio.belajarislam.R;

public class DetailItemActivity extends AppCompatActivity {

    @BindView(R.id.detail_gambar)
    ImageView detail_gambar;
    @BindView(R.id.judul)
    TextView text_judul;
    @BindView(R.id.deskripsi)
    TextView text_deskripsi;

    private String id;
    private String kategori;
    private String judul;
    private String deskripsi;
    private DatabaseReference databaseReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_item);
        ButterKnife.bind(this);

        Bundle bundle = getIntent().getExtras();
        id = bundle.getString("id");
        kategori = bundle.getString("kategori");
        judul = bundle.getString("judul");
        deskripsi = bundle.getString("deskripsi");
        text_judul.setText(judul);
        text_deskripsi.setText(deskripsi);
        databaseReference = FirebaseDatabase.getInstance().getReference("artikels").child(kategori);

        if(kategori.equals("Fikih Doa dan Dzikir")){

            detail_gambar.setImageResource(R.drawable.photo_doa);

        }else if(kategori.equals("Kumpulan Doa dan Dzikir")){

            detail_gambar.setImageResource(R.drawable.photo_doa);

        }else if(kategori.equals("Biografi Narasumber")){

            detail_gambar.setImageResource(R.drawable.photo_doa);

        }
    }

    @OnClick(R.id.back)
    public void back() {
        onBackPressed();
    }

    @OnClick(R.id.menu)
    public void menu(final View view) {
        Context wrapper = new ContextThemeWrapper(DetailItemActivity.this, R.style.MyPopupMenu);
        PopupMenu popup = new PopupMenu(wrapper, view);
        popup.getMenuInflater().inflate(R.menu.menu_list, popup.getMenu());
        popup.show();
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.bagi:
                        Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                        sharingIntent.setType("text/plain");
                        sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Belajar Islam");
                        sharingIntent.putExtra(android.content.Intent.EXTRA_TITLE, judul);
                        sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, deskripsi);
                        sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                        startActivity(Intent.createChooser(sharingIntent, "Share via").setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                        break;
//                    case R.id.edit:
//                        AlertDialog.Builder alertEdit = new AlertDialog.Builder(DetailItemActivity.this, R.style.customAlertDialog);
//                        alertEdit.setMessage("Artikel akan diedit?");
//                        alertEdit.setCancelable(false);
//                        alertEdit.setPositiveButton("YA", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                Intent intent = new Intent(getApplicationContext(), EditActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                intent.putExtra("id", id);
//                                intent.putExtra("kategori",kategori);
//                                intent.putExtra("judul", judul);
//                                intent.putExtra("deskripsi", deskripsi);
//                                startActivity(intent);
//                            }
//                        });
//                        alertEdit.setNegativeButton("KEMBALI", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                            }
//                        });
//                        alertEdit.show();
//                        break;
//                    case R.id.hapus:
//                        AlertDialog.Builder alertHapus = new AlertDialog.Builder(DetailItemActivity.this, R.style.customAlertDialog);
//                        alertHapus.setMessage("Artikel akan dihapus?");
//                        alertHapus.setCancelable(false);
//                        alertHapus.setPositiveButton("YA", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                databaseReference.child(id).removeValue();
//                                onBackPressed();
//                                finish();
//                                Snackbar.make(view, "Artikel berhasil dihapus", Snackbar.LENGTH_LONG).show();
//                            }
//                        });
//                        alertHapus.setNegativeButton("KEMBALI", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int which) {
//                                dialog.dismiss();
//                            }
//                        });
//                        alertHapus.show();
//                        break;
                    default:
                        break;
                }

                return true;
            }
        });
    }
}
