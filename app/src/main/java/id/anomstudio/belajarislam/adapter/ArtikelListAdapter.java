package id.anomstudio.belajarislam.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.pixplicity.sharp.Sharp;

import java.util.ArrayList;
import java.util.List;

import id.anomstudio.belajarislam.R;
import id.anomstudio.belajarislam.adapter.holder.ArtikelListHolder;
import id.anomstudio.belajarislam.model.ArtikelModel;
import id.anomstudio.belajarislam.ui.menu.beranda.detail.DetailItemActivity;
import id.anomstudio.belajarislam.ui.menu.beranda.detail.DetailKategoriActivity;

/**
 * Created by Pluto on 21/01/2018.
 */

public class ArtikelListAdapter extends RecyclerView.Adapter<ArtikelListHolder> {
    private List<ArtikelModel> mItems;
    private Activity context;
    private DatabaseReference databaseReference;

    public ArtikelListAdapter(Activity context, List<ArtikelModel> mItems) {
        this.context = context;
        this.mItems = mItems;

    }

    @Override
    public ArtikelListHolder onCreateViewHolder(ViewGroup parent, int i) {
        ArtikelListHolder holder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.item_artikel, parent, false);

        if (String.valueOf(DetailKategoriActivity.fikih_doa_dzikir).equals("Fikih Doa dan Dzikir")) {
            ImageView gambar = view.findViewById(R.id.gambar);
            Sharp.loadResource(context.getResources(), R.raw.ic_muslim_salat).into(gambar);
            holder = new ArtikelListHolder(view);
        } else if (String.valueOf(DetailKategoriActivity.kumpulan_doa).equals("Kumpulan Doa dan Dzikir")) {
            ImageView gambar = view.findViewById(R.id.gambar);
            Sharp.loadResource(context.getResources(), R.raw.ic_muslim_book).into(gambar);
            holder = new ArtikelListHolder(view);
        } else if (String.valueOf(DetailKategoriActivity.biografi_narasumber).equals("Biografi Narasumber")) {
            ImageView gambar = view.findViewById(R.id.gambar);
            Sharp.loadResource(context.getResources(), R.raw.ic_muslim_mosque).into(gambar);
            holder = new ArtikelListHolder(view);
        }

        return holder;
    }

    @Override
    public void onBindViewHolder(final ArtikelListHolder holder, final int position) {
        final ArtikelModel artikel = mItems.get(position);
        databaseReference = FirebaseDatabase.getInstance().getReference("artikels").child(artikel.getKategori());
        holder.setContent(mItems.get(position).getJudul());
        holder.mMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                Context wrapper = new ContextThemeWrapper(context, R.style.MyPopupMenu);
                PopupMenu popup = new PopupMenu(wrapper, view);
                popup.getMenuInflater().inflate(R.menu.menu_list, popup.getMenu());
                popup.show();
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.bagi:
                                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Belajar Islam");
                                sharingIntent.putExtra(android.content.Intent.EXTRA_TITLE, mItems.get(holder.getAdapterPosition()).getJudul());
                                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, mItems.get(holder.getAdapterPosition()).getDeskripsi());
                                sharingIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                                context.startActivity(Intent.createChooser(sharingIntent, "Share via").setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
                                break;
//                            case R.id.edit:
//                                AlertDialog.Builder alertEdit = new AlertDialog.Builder(context, R.style.customAlertDialog);
//                                alertEdit.setMessage("Artikel akan diedit?");
//                                alertEdit.setCancelable(false);
//                                alertEdit.setPositiveButton("YA", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        Intent intent = new Intent(context, EditActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                                        intent.putExtra("id", String.valueOf(artikel.getId()));
//                                        intent.putExtra("kategori", artikel.getKategori());
//                                        intent.putExtra("judul", artikel.getJudul());
//                                        intent.putExtra("deskripsi", artikel.getDeskripsi());
//                                        context.startActivity(intent);
//                                    }
//                                });
//                                alertEdit.setNegativeButton("KEMBALI", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                                alertEdit.show();
//                                break;
//                            case R.id.hapus:
//                                AlertDialog.Builder alertHapus = new AlertDialog.Builder(context, R.style.customAlertDialog);
//                                alertHapus.setMessage("Artikel akan dihapus?");
//                                alertHapus.setCancelable(false);
//                                alertHapus.setPositiveButton("YA", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        databaseReference.child(artikel.getId()).removeValue();
//                                        mItems.remove(holder.getAdapterPosition());
//                                        notifyItemRemoved(holder.getAdapterPosition());
//                                        Snackbar.make(view, "Artikel berhasil dihapus", Snackbar.LENGTH_LONG).show();
//                                    }
//                                });
//                                alertHapus.setNegativeButton("KEMBALI", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int which) {
//                                        dialog.dismiss();
//                                    }
//                                });
//                                alertHapus.show();
//                                break;
                            default:
                                break;
                        }

                        return true;
                    }
                });
            }
        });
        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, DetailItemActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("id", artikel.getId());
                intent.putExtra("kategori", artikel.getKategori());
                intent.putExtra("judul", artikel.getJudul());
                intent.putExtra("deskripsi", artikel.getDeskripsi());
                context.startActivity(intent);
            }
        });

        holder.itemView.setTag(artikel);
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void filterList(ArrayList<ArtikelModel> filteredList){
        mItems = filteredList;
        notifyDataSetChanged();
    }

}
