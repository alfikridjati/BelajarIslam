package id.anomstudio.belajarislam.adapter.holder;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.loopeer.shadow.ShadowView;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.anomstudio.belajarislam.R;

/**
 * Created by Pluto on 21/01/2018.
 */

public class ArtikelListHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

    private final Context context;
    @BindView(R.id.judul)
    public TextView mJudul;
    @BindView(R.id.more)
    public ImageButton mMore;
    @BindView(R.id.shadow1)
    public ShadowView mView;


    public ArtikelListHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        context = itemView.getContext();

        itemView.setOnClickListener(this);
    }

    public void setContent(String judul){
        mJudul.setText(judul);
    }

    @Override
    public void onClick(View view) {
//        Intent intent = new Intent(context, DetailItemActivity.class);
//        Artikel artikel = (Artikel) itemView.getTag();
//        intent.putExtra("id",artikel.getId());
//        intent.putExtra("kategori",artikel.getKategori());
//        intent.putExtra("judul",artikel.getJudul());
//        intent.putExtra("deskripsi",artikel.getDeskripsi());
//        context.startActivity(intent);
    }
}
